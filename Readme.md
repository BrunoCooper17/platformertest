# PlatformerTest
Este proyecto fue creado como parte de una demostracion en vivo para el evento **j4Guanatos**.
El proyecto fue creado en [Unreal Engine 4](https://www.unrealengine.com/en-US/what-is-unreal-engine-4) (version 4.22) usando unicamente Blueprints.

Realidado por:
 - Jesús Mastache Caballero (BrunoCooper17)

[Portafolio](https://brunocooper17.gitlab.io/portafolio/projects)
[Twitter](https://twitter.com/BrunoCooper_17)
[LinkedIn](https://www.linkedin.com/in/jesus-mastache-25265952/)
